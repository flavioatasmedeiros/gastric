var _ = require('lodash');
var Template = require('./template');
var grunt  = require('grunt');

module.exports = function(task) {

  var template = Template(task);

  var write = function(fileName, templateName, context) {

    var filePath = task.target + '/' + fileName;
    var html = template.render(templateName, context);

    grunt.file.write(filePath, html);
  };

  return write;

};
