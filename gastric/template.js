var nunjucks = require('nunjucks');

module.exports = function(task) {
  
  var options = task.options(); 

  nunjucks.configure(options.dirs.templates);

  return {
    render: function(name, context) {
      return nunjucks.render(name + '.html', context);
    }
  };

};
