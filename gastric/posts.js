var Utils = require('./utils');
var Files = require('../lib/files');

module.exports = function(task) {

  var criteria  = task.options().fetchCriteria;
  var injectors = Utils(task);
  var fetch     = Files(task)[criteria];

  return {
    fetch: function() {
      return fetch().map(injectors);
    }
  };

};
