var _ = require('lodash');

var mapCompose = function(list, func, context) {
  return _.compose.apply(
    context || null, list.map(func)
  );
};

module.exports = function(task) {
  var injectors = task.options().injectors;

  return mapCompose(injectors, function(name) {
    return require(name)(task);
  });
};
