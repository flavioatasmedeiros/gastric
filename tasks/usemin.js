module.exports = function(grunt) {

  grunt.config.set('usemin', {
    options: {
      assetsDirs: ['dist']
    },
    html: ['dist/{,*/}*.html'],
    css: ['dist/styles/{,*/}*.css']
  });


}
