var Posts     = require('../gastric/posts');
var Generator = require('../gastric/generator');

module.exports = function(grunt) {
  
  grunt.registerMultiTask('posts', 'Process all posts.', function() {

    var generator = Generator(this);
  
    var posts = Posts(this).fetch();

    generator('index.html', 'home', {posts: posts});
    
    posts.forEach(function(post) {
      generator(post.slug + '/index.html', 'post', post);  
    });
  
  });

};