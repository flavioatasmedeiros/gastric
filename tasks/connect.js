module.exports = function(grunt) {

  grunt.config.set('connect', {
    options: { 
      port: 9000,
      livereload: 27664,
      hostname: 'localhost'
    },
    livereload: {
      options: {
      open: true,
      base: ['app', '.tmp',]
      }
    }
  });
};
