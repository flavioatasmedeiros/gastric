module.exports = function(grunt) {

  grunt.config.set('rsync', {
    options: {
      args: ["--verbose"],
      exclude: [".git*","*.scss","node_modules"],
      recursive: true
    },
    dist: {
      options: {
        host: "sigmus@do.xpto.me",
        src: "dist",
        dest: "/home/sigmus/sites/main/"
      }
    }
  });

}