module.exports = function(grunt) {

  grunt.config.set('watch', {
    posts: {
      files: ['posts/*.*', 'app/templates/*.html', 'tasks/*.*'],
      tasks: ['posts:.tmp']
    },
    less: {
      files: ['app/styles/{,*/}*.less'],
      tasks: ['less:server']
    },
    styles: {
      files: ['app/styles/{,*/}*.css'],
      tasks: ['copy:styles']
    },
    livereload: {
      options: {
        livereload: '<%= connect.options.livereload %>'
      },
      files: [
        'posts/*.*',
        'tasks/*.*',
        'app/templates/*.html',
        'app/styles/*',
      ]
    }
  });
}