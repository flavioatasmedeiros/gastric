module.exports = function(grunt) {

  grunt.config.set('less', {
    options: {
      paths: ['app/bower_components'],
    },
    dist: {
      options: {
        yuicompress: true,
        report: 'gzip'
      },
      files: {
        'dist/styles/main.css': 'app/styles/main.less'
      }
    },
    server: {
      options: {
        dumpLineNumbers: 'all'
      },
      files: {
        '.tmp/styles/main.css': 'app/styles/main.less'
      }
    }
  });

}
