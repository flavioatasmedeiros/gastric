module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt); 

  grunt.initConfig({
    posts: {
      options: {
        date: {
          format: {
            input: 'YYYY-MM-DD',
            output: 'LL',
          }
        },
        lang: 'pt-BR',
        injectors: ['../lib/markdown', '../lib/date'],
        source: "posts/index.json",
        dirs: {
          templates: 'app/templates/',
          posts: 'posts/',
        }
      },
      '.tmp': {
        options: {
          fetchCriteria: 'all'
        }
      },
      'dist': {
        options: {
          fetchCriteria: 'published',
        }
      },
    },
  });

  grunt.loadTasks('tasks');

  grunt.registerTask('server', [
    'clean:.tmp', 
    'posts:.tmp',
    'less:server',
    'connect:livereload',
    'watch',
  ]);

  grunt.registerTask('build', [
    'clean:dist', 
    'less:dist',
    'posts:dist',
    'rsync:dist',
  ]);

}