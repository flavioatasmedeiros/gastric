
<br>Adaptado de *[Know Your Next Commit](http://programmer.97things.oreilly.com/wiki/index.php/Know_Your_Next_Commit)* — Bati no ombro de três programadores e perguntei o que estavam fazendo. “Estou refactorando estes métodos”, respondeu o primeiro. “Estou adicionando alguns parâmetros nesta web action”, o segundo explicou. O terceiro respondeu: “Estou trabalhando nesta [história de usuário](http://en.wikipedia.org/wiki/User_story)”.

A principio pode parecer que os dois primeiros estavam absorvidos pelos detalhes do trabalho, enquanto o último conseguia enxergar o problema de uma forma mais global. Entretanto, quando eu perguntei quando e o que iriam commitar, o cenário mudou drasticamente. 

Os dois primeiros foram bem claros sobre quais arquivos estariam envolvidos e o que seria terminado em mais ou menos uma hora. O terceiro programador respondeu: “Ah, eu acho que vou terminar daqui há alguns dias. Vou provavelmente adicionar algumas classes e isso pode mudar os serviços de alguma forma”.

Aos primeiros não faltava uma visão geral do objetivo. Escolheram tarefas que apontavam para uma direção produtiva e julgavam seriam terminadas em aproximadamente uma hora.  Uma vez terminadas, escolheriam uma nova funcionalidade ou refactorariam para poder continuar. Todo o código era portanto escrito com um propósito claro e delimitado, com uma meta alcançável em vista.

### Programação especulativa

O terceiro programador não foi capaz de decompor o problema e estava trabalhando em todos os aspectos ao mesmo tempo. Não tinha ideia do que era preciso ser feito. Estava essencialmente fazendo uma programação especulativa, esperando que em algum momento pudesse commitar o seu código. Muito provavelmente o código escrito no começo da longa sessão de programação se encaixava de uma maneira pobre com a solução finalmente alcançada.

O que fariam os dois primeiros programadores se as suas tarefas demorassem mais do que duas horas? Após perceberem que abraçaram coisas demais eles provavelmente jogariam fora muitas das mudanças, definiriam tarefas menores e recomeçariam. Continuar trabalhando seria perder o foco código especulativo entraria no repositório. Ao invés disso, mudanças seriam descartadas mas os insights permaneceriam.

O terçeiro programador pode continuar adivinhando e desesperadamente tentando remendar suas alterações até formar algo que possa ser commitado. Afinal, descartar mudanças já feitas no código é trabalho jogado fora, não? Infelizmente não descartar código pode levar a um código esquisito, sem um propósito claro.

### Especulando conscientemente

Eventualmente até programadores focados em commits podem falhar. Um código que parecia poder ser feito em duas horas pode acabar sendo inútil. Percebendo isso podem entrar diretamente no modo especulativo, brincando com o código e jogando fora muitas mudanças até que um insight coloca as coisas de volta aos trilhos. Momentos desestruturados tem também seu propósito: aprender sobre o código e ser capaz de definir uma tarefa que constitua uma evolução.

Tenha sempre mente seu próximo commit. Se não conseguir terminar, descarte as mudanças e defina uma nova tarefa mais objetiva a partir dos insights obtidos. Faça experiências especulativas sempre que necessário, mas não se deixe entrar neste modo sem perceber. Não commite trabalho de adivinhação no seu repositório.