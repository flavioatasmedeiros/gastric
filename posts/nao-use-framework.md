<br>
Alguns clientes resistem à utilização de bibliotecas ou frameworks. Tento entender
os possíveis cenários que fazem isso acontecer mas em determinados casos fica difícil ser 
solidário e acaba parecendo muito mais uma imposição arbritária de profissionais preguiçosos
e medrosos, uma resposta padrão pronta.

### Padrões de desenvolvimento do cliente

Se o cliente já utiliza um determinado framework faz sentido pedir para que a
empresa contratada siga os padrões já previamente determinados. Mas existindo um espaço
a ser preenchido por uma biblioteca ou framework comprovadamente eficaz e bem documentado
esta opção, para ser descartada, precisa de motivos sólidos.

Digo isso porque a alternativa vai ser um código específicamente feito para aquele projeto ou,
pior ainda, um framework proprietário. Nos dois casos, somente quem fez o código original pode 
ajudar o programador que vai manter o código. 

Mesmo que o resultado final seja muito bem documentado e organizado, é ousado afirmar que 
não utilizar um framework é mais seguro do que utilizar um projeto open source 
com centenas de colaboradores e milhares de usuários. Me parece justamente o contrário.


