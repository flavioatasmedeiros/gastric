var grunt = require('grunt');

module.exports = function(task) {

  var options = task.options();

  var isPublished = function(post) {
    return post.published;
  };

  var all = function() {
    var json = grunt.file.read(options.source);
    return JSON.parse(json);
  };

  return {
    all: all,
    published: function() {
      return all().filter(isPublished);
    }
  };

};
