var grunt  = require('grunt');
var marked = require('marked');

module.exports = function(task) {
  
  var postDir = task.options().dirs.posts;

  return function(post) {
    
    var filePath = postDir + post.slug + '.md';

    var content = grunt.file.read(filePath);
    
    post.html = marked(content);
    
    return post;
  };

};