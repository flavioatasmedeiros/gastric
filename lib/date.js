var moment = require('moment');

module.exports = function(task) {
  
  var options = task.options();

  moment.lang(options.lang);

  return function(post) {

    var format = options.date.format;
    
    var date = moment(post.date, format.input);
    
    post.when = date.format(format.output);
    
    return post;
  };

};
